<?php

class Neklo_Asf_Block_Adminhtml_Allmethod
    extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_allmethod';
        $this->_blockGroup = 'neklo_asf';
        $this->_headerText = Mage::helper('neklo_asf')->__('Shipping Methods');
        $this->_addButtonLabel = Mage::helper('neklo_asf')->__('Add New Method');
        parent::__construct();
    }

}
