<?php

class Neklo_Asf_Block_Adminhtml_Allmethod_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_allmethod';
        $this->_blockGroup = 'neklo_asf';

        parent::__construct();

        $this->_updateButton(
            'save', 'label', Mage::helper('neklo_asf')->__('Save Method')
        );
        $this->_updateButton(
            'delete', 'label', Mage::helper('neklo_asf')->__('Delete Method')
        );

        $this->_addButton(
            'saveandcontinue', array(
            'label'   => Mage::helper('adminhtml')->__(
                'Save and Continue Edit'
            ),
            'onclick' => 'saveAndContinueEdit()',
            'class'   => 'save',
        ), -100
        );

        $this->_formScripts[]
            = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
            function saveAndAddRate(){
                editForm.submit($('edit_form').action+'back/addrate/method/"
            . $this->getRequest()->getParam('id') . "');
            }
        ";
    }

    public function getHeaderText()
    {
        if (Mage::registry('nekloasf_method')->getId()) {
            return Mage::helper('neklo_asf')->__(
                "Edit %s Method",
                $this->escapeHtml(Mage::registry('nekloasf_method')->getName())
            );
        } else {
            return Mage::helper('neklo_asf')->__("New Method");
        }
    }

}
