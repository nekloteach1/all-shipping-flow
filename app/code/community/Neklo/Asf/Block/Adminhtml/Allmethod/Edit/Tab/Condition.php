<?php
class Neklo_Asf_Block_Adminhtml_Allmethod_Edit_Tab_Condition
    extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('nekloasf_method');

        $form = new Varien_Data_Form();

        $model->getConditions()->setJsFormObject('condition_fieldset');

        $renderer = Mage::getBlockSingleton('adminhtml/widget_form_renderer_fieldset')
            ->setTemplate('promo/fieldset.phtml')
            ->setNewChildUrl($this->getUrl('*/promo_quote/newConditionHtml/form/condition_fieldset'));

        $conditionFieldSet = $form->addFieldset('condition_fieldset', array(
            'legend'=>Mage::helper('neklo_asf')->__('Apply the rule only if the following conditions are met (leave blank for all products)')
        ))->setRenderer($renderer);

        $conditionFieldSet->addField('conditions', 'text', array(
            'name' => 'conditions',
            'label' => Mage::helper('neklo_asf')->__('Conditions'),
            'title' => Mage::helper('neklo_asf')->__('Conditions'),
        ))->setRule($model)->setRenderer(Mage::getBlockSingleton('rule/conditions'));


        $this->setForm($form);

        return parent::_prepareForm();
    }
}