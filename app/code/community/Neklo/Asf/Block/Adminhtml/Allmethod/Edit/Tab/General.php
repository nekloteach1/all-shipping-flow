<?php

class Neklo_Asf_Block_Adminhtml_Allmethod_Edit_Tab_General
    extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('nekloasf_method');

        $form = new Varien_Data_Form();

        $fieldset = $form->addFieldset(
            'general_form', array('legend' => Mage::helper('neklo_asf')->__(
                'General Information'
            ))
        );

        $fieldset->addField(
            'name', 'text', array(
                'name'     => 'name',
                'label'    => Mage::helper('neklo_asf')->__('Method Name'),
                'title'    => Mage::helper('neklo_asf')->__('Method Name'),
                'required' => true,
            )
        );

        $fieldset->addField(
            'active', 'select', array(
                'name'     => 'active',
                'label'    => Mage::helper('neklo_asf')->__('Method Status'),
                'title'    => Mage::helper('neklo_asf')->__('Method Status'),
                'required' => true,
                'options'  => Mage::getModel('neklo_asf/source_status')
                    ->toArray(),
            )
        );

        $fieldset->addField(
            'virtual_product', 'select', array(
                'name'     => 'virtual_product',
                'label'    => Mage::helper('neklo_asf')->__('Include Virtual'),
                'title'    => Mage::helper('neklo_asf')->__('Include Virtual'),
                'required' => true,
                'options'  => Mage::getModel('neklo_asf/source_virtual')
                    ->toArray(),
            )
        );


        $form->setValues($model->getData());
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
