<?php

class Neklo_Asf_Block_Adminhtml_Allmethod_Edit_Tab_Rates
    extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('ratesGrid');
        $this->setSaveParametersInSession(false);
        $this->setUseAjax(true);
    }

    public function getMainButtonsHtml()
    {
        $html = parent::getMainButtonsHtml();
        $addButton = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(
                array(
                    'label'   => Mage::helper('adminhtml')->__('Add new rate'),
                    'onclick' => "saveAndAddRate()",
                    'class'   => 'add'
                )
            )->toHtml();
        return $addButton . $html;
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getModel('neklo_asf/rates')
            ->getCollection()
            ->addFieldToFilter(
                'method_id', $this->getRequest()->getParam('id')
            );
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn(
            'country', array(
                'header' => Mage::helper('neklo_asf')->__('Country'),
                'align'  => 'left',
                'index'  => 'country',
            )
        );

        $this->addColumn(
            'state', array(
                'header' => Mage::helper('neklo_asf')->__('State'),
                'align'  => 'left',
                'index'  => 'state'
            )
        );

        $this->addColumn(
            'city', array(
                'header' => Mage::helper('neklo_asf')->__('City'),
                'align'  => 'left',
                'index'  => 'city'
            )
        );

        $this->addColumn(
            'zip_from', array(
                'header' => Mage::helper('neklo_asf')->__('Zip from'),
                'align'  => 'left',
                'index'  => 'zip_from',
            )
        );

        $this->addColumn(
            'zip_to', array(
                'header' => Mage::helper('neklo_asf')->__('Zip to'),
                'align'  => 'left',
                'index'  => 'zip_to',
            )
        );

        $this->addColumn(
            'subtotal_from', array(
                'header'   => Mage::helper('neklo_asf')->__('Subtotal from, $'),
                'align'    => 'left',
                'renderer' => 'Neklo_Asf_Block_Adminhtml_Allrates_Edit_Renderer_NullGrid',
                'index'    => 'subtotal_from',
            )
        );

        $this->addColumn(
            'subtotal_to', array(
                'header'   => Mage::helper('neklo_asf')->__('Subtotal to, $'),
                'align'    => 'left',
                'renderer' => 'Neklo_Asf_Block_Adminhtml_Allrates_Edit_Renderer_NullGrid',
                'index'    => 'subtotal_to',
            )
        );

        $this->addColumn(
            'qty_from', array(
                'header'   => Mage::helper('neklo_asf')->__('Qty from'),
                'align'    => 'left',
                'renderer' => 'Neklo_Asf_Block_Adminhtml_Allrates_Edit_Renderer_NullGrid',
                'index'    => 'qty_from',
            )
        );

        $this->addColumn(
            'qty_to', array(
                'header'   => Mage::helper('neklo_asf')->__('Qty to'),
                'align'    => 'left',
                'renderer' => 'Neklo_Asf_Block_Adminhtml_Allrates_Edit_Renderer_NullGrid',
                'index'    => 'qty_to',
            )
        );

        $this->addColumn(
            'weight_from', array(
                'header'   => Mage::helper('neklo_asf')->__('Weight from'),
                'align'    => 'left',
                'renderer' => 'Neklo_Asf_Block_Adminhtml_Allrates_Edit_Renderer_NullGrid',
                'index'    => 'weight_from',
            )
        );

        $this->addColumn(
            'weight_to', array(
                'header'   => Mage::helper('neklo_asf')->__('Weight to'),
                'align'    => 'left',
                'renderer' => 'Neklo_Asf_Block_Adminhtml_Allrates_Edit_Renderer_NullGrid',
                'index'    => 'weight_to',
            )
        );


        $this->addColumn(
            'per_order', array(
                'header' => Mage::helper('neklo_asf')->__('Per order, $'),
                'align'  => 'left',
                'index'  => 'per_order',
            )
        );

        $this->addColumn(
            'per_qty', array(
                'header' => Mage::helper('neklo_asf')->__('Per qty, $'),
                'align'  => 'left',
                'index'  => 'per_qty',
            )
        );

        $this->addColumn(
            'per_price', array(
                'header' => Mage::helper('neklo_asf')->__('Per price, %'),
                'align'  => 'left',
                'index'  => 'per_price',
            )
        );

        $this->addColumn(
            'per_weight', array(
                'header' => Mage::helper('neklo_asf')->__('Per weight, $'),
                'align'  => 'left',
                'index'  => 'per_weight',
            )
        );

        $this->addColumn(
            'shipping_type', array(
                'header'  => Mage::helper('neklo_asf')->__('Shipping Type'),
                'align'   => 'left',
                'type'    => 'options',
                'options' => Mage::getModel('neklo_asf/source_shippingtype')
                    ->toArray(),
                'index'   => 'shipping_type'
            )
        );

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/allrates/edit', array('id' => $row->getId()));
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

}
