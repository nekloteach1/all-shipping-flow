<?php

class Neklo_Asf_Block_Adminhtml_Allmethod_Edit_Tabs
    extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('method_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('neklo_asf')->__('Method Information'));
    }

    protected function _prepareLayout()
    {
        $this->addTab(
            'general_tab', array(
                'label'   => Mage::helper('neklo_asf')->__('General'),
                'title'   => Mage::helper('neklo_asf')->__('General'),
                'content' => $this->getLayout()->createBlock(
                    'neklo_asf/adminhtml_allmethod_edit_tab_general'
                )->toHtml(),
            )
        );

        $this->addTab(
            'rates_tab', array(
                'label' => Mage::helper('neklo_asf')->__('Rates'),
                'title' => Mage::helper('neklo_asf')->__('Rates'),
                'content' => $this->getLayout()->createBlock(
                    'neklo_asf/adminhtml_allmethod_edit_tab_rates'
                )->toHtml(),
            )
        );

        $this->addTab(
            'condition_tab', array(
                'label'   => Mage::helper('neklo_asf')->__('Conditions'),
                'title'   => Mage::helper('neklo_asf')->__('Conditions'),
                'content' => $this->getLayout()->createBlock(
                    'neklo_asf/adminhtml_allmethod_edit_tab_condition'
                )->toHtml(),
            )
        );

        return parent::_prepareLayout();
    }
}
