<?php

class Neklo_Asf_Block_Adminhtml_Allmethod_Grid
    extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('id');
        $this->setDefaultSort('name');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('neklo_asf/method')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'name', array(
                'header' => Mage::helper('neklo_asf')->__('Name'),
                'align'  => 'left',
                'type'   => 'text',
                'index'  => 'name'
            )
        );

        $this->addColumn(
            'active', array(
                'header'  => Mage::helper('neklo_asf')->__('Active'),
                'align'   => 'left',
                'type'    => 'options',
                'options' => Mage::getModel('neklo_asf/source_status')->toArray(
                ),
                'index'   => 'active'
            )
        );

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }

}
