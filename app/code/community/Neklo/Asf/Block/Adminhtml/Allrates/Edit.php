<?php

class Neklo_Asf_Block_Adminhtml_Allrates_Edit
    extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'id';
        $this->_controller = 'adminhtml_allrates';
        $this->_blockGroup = 'neklo_asf';

        parent::__construct();

        $this->_updateButton(
            'save', 'label', Mage::helper('neklo_asf')->__('Save Rate')
        );
        $this->_updateButton(
            'delete', 'label', Mage::helper('neklo_asf')->__('Delete Rate')
        );

        $this->_addButton(
            'saveandcontinue', array(
            'label'   => Mage::helper('adminhtml')->__(
                'Save and Continue Edit'
            ),
            'onclick' => 'saveAndContinueEdit()',
            'class'   => 'save',
        ), -100
        );

        $this->_formScripts[]
            = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('rates')->getId()) {
            return Mage::helper('neklo_asf')->__(
                "Edit Rate id %s",
                $this->escapeHtml(Mage::registry('rates')->getId())
            );
        } else {
            return Mage::helper('neklo_asf')->__('New Rate');
        }
    }

    public function getBackUrl()
    {
        parent::getBackUrl();
        if (Mage::registry('rates')->getMethodId()) {
            return $this->getUrl(
                '*/allmethod/edit',
                array('id' => Mage::registry('rates')->getMethodId())
            );
        } else {
            return $this->getUrl(
                '*/allmethod/'
            );
        }
    }

}
