<?php

class Neklo_Asf_Block_Adminhtml_Allrates_Edit_Form
    extends Mage_Adminhtml_Block_Widget_Form
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('rate_form');
        $this->setTitle(Mage::helper('neklo_asf')->__('Rate Information'));
        $this->_allowCountries = Mage::getStoreConfig(
            'carriers/nekloasf/specificcountry',
            Mage::app()->getStore()
        );
    }

    protected function _prepareForm()
    {
        $model = Mage::registry('rates');
        if (!$model->getId()) {
            $model->setCity('*');
            $model->setState('*');
            $model->setZipFrom('*');
            $model->setZipTo('*');
            $model->setSubtotalTo('0.0000');
            $model->setSubtotalFrom('0.0000');
            $model->setQtyTo('0.0000');
            $model->setQtyFrom('0.0000');
            $model->setWeightTo('0.0000');
            $model->setWeightFrom('0.0000');
            $model->setPerOrder('0.0000');
            $model->setPerQty('0.0000');
            $model->setPerPrice('0.0000');
            $model->setPerWeight('0.0000');
            $model->setMethodId($this->getRequest()->getParam('method'));
        }

        $form = new Varien_Data_Form(
            array(
                'id'     => 'edit_form',
                'action' => $this->getUrl(
                    '*/*/save',
                    array('id' => $this->getRequest()->getParam('id'))
                ),
                'method' => 'post')
        );


        $fieldset = $form->addFieldset(
            'base_fieldset',
            array('legend' => Mage::helper('neklo_asf')->__('Rate Information'),
                  'class'  => 'fieldset-wide')
        );

        if ($model->getBlockId()) {
            $fieldset->addField(
                'id', 'hidden', array(
                    'name' => 'id',
                )
            );
        }

        $regions = Mage::getModel('directory/country')
            ->getLoadedRegionCollection()->getData();
        foreach ($regions as $region) {
            $allRegion[$region['country_id']][$region['code']] = $region['name'];
        }
        $region = json_encode($allRegion);
        $state = $model->getData('state');

        $fieldset->addField(
            'method_id', 'select', array(
                'name'  => 'method_id',
                'label' => Mage::helper('neklo_asf')->__('Method'),
                'title' => Mage::helper('neklo_asf')->__('Method'),
                'required' => true,
                'values'=> Mage::getModel('neklo_asf/source_methods')
                    ->toOptionArray()
            )
        );

        $fieldset->addField(
            'shipping_type', 'select', array(
                'name'  => 'shipping_type',
                'label' => Mage::helper('neklo_asf')->__('Shipping Type'),
                'title' => Mage::helper('neklo_asf')->__('Shipping Type'),
                'required' => true,
                'values'=> Mage::getModel('neklo_asf/source_shippingtype')
                    ->toOptionArray()
            )
        );

        $country = $fieldset->addField(
            'country', 'select', array(
                'name'     => 'country',
                'label'    => Mage::helper('neklo_asf')->__('Country'),
                'title'    => Mage::helper('neklo_asf')->__('Country'),
                'required' => true,
                'onchange' => 'showHideField()',
                'values'   => Mage::getModel('neklo_asf/source_country')
                    ->toOptionArray($this->_allowCountries),

            )
        );


        $fieldset->addField(
            'state', 'select', array(
                'name'     => 'state',
                'label'    => Mage::helper('neklo_asf')->__('State'),
                'title'    => Mage::helper('neklo_asf')->__('State'),
                'values'   => Mage::getModel('neklo_asf/source_regions')
                    ->toOptionArray($model->getCountry()),
                'required' => true,
            )
        );

        $country->setAfterElementHtml(
            "<script>
                    var Regions = " . $region . ";
                    var State = '" . $state . "';
                    function showHideField(){
                        var Country = document.getElementById('country').value;
                        var el = document.getElementById('state');
                        var opt = document.createElement('option');
                        while(el.childNodes.length >=1) {
                            el.removeChild(el.firstChild);
                        }
                            opt.innerHTML='*';
                            opt.setAttribute('value', '*');
                            el.appendChild(opt);
                       for(var k in window.Regions[Country]) {

                           var opt = document.createElement('option');

                            opt.innerHTML=window.Regions[Country][k];
                            opt.setAttribute('value', k);
                            if(k == window.State){
                               opt.selected = true;
                           }
                            el.appendChild(opt);
                            
                        }

                    }

                    </script>"
        );


        $fieldset->addField(
            'city', 'text', array(
                'name'     => 'city',
                'label'    => Mage::helper('neklo_asf')->__('City'),
                'title'    => Mage::helper('neklo_asf')->__('City'),
                'required' => true,
            )
        );

        $fieldset->addField(
            'zip_from', 'text', array(
                'name'     => 'zip_from',
                'label'    => Mage::helper('neklo_asf')->__('Zip from'),
                'title'    => Mage::helper('neklo_asf')->__('Zip from'),
                'required' => true,
            )
        );

        $fieldset->addField(
            'zip_to', 'text', array(
                'name'     => 'zip_to',
                'label'    => Mage::helper('neklo_asf')->__('Zip to'),
                'title'    => Mage::helper('neklo_asf')->__('Zip to'),
                'required' => true,
            )
        );
        $fieldset->addType('typenull', 'Neklo_Asf_Block_Adminhtml_Allrates_Edit_Renderer_NullForm');
        $fieldset->addField(
            'subtotal_from', 'typenull', array(
                'name'  => 'subtotal_from',
                'label' => Mage::helper('neklo_asf')->__('Subtotal from, $'),
                'title' => Mage::helper('neklo_asf')->__('Subtotal from, $'),
            )
        );

        $fieldset->addField(
            'subtotal_to', 'typenull', array(
                'name'  => 'subtotal_to',
                'label' => Mage::helper('neklo_asf')->__('Subtotal to, $'),
                'title' => Mage::helper('neklo_asf')->__('Subtotal to, $'),
            )
        );

        $fieldset->addField(
            'qty_from', 'typenull', array(
                'name'  => 'qty_from',
                'label' => Mage::helper('neklo_asf')->__('Qty from'),
                'title' => Mage::helper('neklo_asf')->__('Qty from'),
            )
        );

        $fieldset->addField(
            'qty_to', 'typenull', array(
                'name'  => 'qty_to',
                'label' => Mage::helper('neklo_asf')->__('Qty to'),
                'title' => Mage::helper('neklo_asf')->__('Qty to'),
            )
        );

        $fieldset->addField(
            'weight_from', 'typenull', array(
                'name'  => 'weight_from',
                'label' => Mage::helper('neklo_asf')->__('Weight from'),
                'title' => Mage::helper('neklo_asf')->__('Weight from'),
            )
        );

        $fieldset->addField(
            'weight_to', 'typenull', array(
                'name'  => 'weight_to',
                'label' => Mage::helper('neklo_asf')->__('Weight to'),
                'title' => Mage::helper('neklo_asf')->__('Weight to'),
            )
        );

        $fieldset->addField(
            'volume_from', 'typenull', array(
                'name'  => 'volume_from',
                'label' => Mage::helper('neklo_asf')->__('Volume from'),
                'title' => Mage::helper('neklo_asf')->__('Volume from'),
            )
        );

        $fieldset->addField(
            'volume_to', 'typenull', array(
                'name'  => 'volume_to',
                'label' => Mage::helper('neklo_asf')->__('Volume to'),
                'title' => Mage::helper('neklo_asf')->__('Volume to'),
            )
        );

        $fieldset->addField(
            'per_order', 'text', array(
                'name'  => 'per_order',
                'class' => 'validate-number',
                'label' => Mage::helper('neklo_asf')->__('Per order, $'),
                'title' => Mage::helper('neklo_asf')->__('Per order, $'),
            )
        );

        $fieldset->addField(
            'per_qty', 'text', array(
                'name'  => 'per_qty',
                'class' => 'validate-number',
                'label' => Mage::helper('neklo_asf')->__('Per qty, $'),
                'title' => Mage::helper('neklo_asf')->__('Per qty, $'),
            )
        );

        $fieldset->addField(
            'per_price', 'text', array(
                'name'  => 'per_price',
                'class' => 'validate-number',
                'label' => Mage::helper('neklo_asf')->__('Per price, %'),
                'title' => Mage::helper('neklo_asf')->__('Per price, %'),
            )
        );

        $fieldset->addField(
            'per_weight', 'text', array(
                'name'  => 'per_weight',
                'class' => 'validate-number',
                'label' => Mage::helper('neklo_asf')->__('Per weight, $'),
                'title' => Mage::helper('neklo_asf')->__('Per weight, $'),
            )
        );

        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
