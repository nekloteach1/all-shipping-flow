<?php

class Neklo_Asf_Block_Adminhtml_Allrates_Edit_Renderer_NullForm
    extends Varien_Data_Form_Element_Abstract
{
    public function __construct($data)
    {
        parent::__construct($data);
        $this->setType('text');
    }
    public function getElementHtml()
    {

        if ($this->getValue() == 0) {
            $html = '<input type="text" id=' . $this->getName() . ' name='
                . $this->getName()
                . ' value="*" class="input-text validate-no-html-tags">';
        } else {
            $html = '<input type="text" id name=' . $this->getName() . ' name='
                . $this->getName() . ' value=' . $this->getValue()
                . ' class="input-text validate-no-html-tags">';
        }
        return $html;
    }
}