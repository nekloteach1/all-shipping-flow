<?php

class Neklo_Asf_Block_Adminhtml_Allrates_Edit_Renderer_NullGrid
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {
        $value =  $row->getData($this->getColumn()->getIndex());
        if ($value == 0) {
            $value = '*';
        }
        $this->setClass('text');
        return $value;

    }

}