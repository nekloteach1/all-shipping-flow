<?php

class Neklo_Asf_Helper_Carrier extends Mage_Core_Helper_Abstract
{
    public function getMethods()
    {
        $collection = Mage::getModel('neklo_asf/method')->getCollection()
            ->AddFieldToFilter(
                'active', array('eq' => Neklo_Asf_Model_Source_Status::ENABLED)
            );
        return $collection;
    }

    public function getRates($cart, $shipType)
    {
        $collection = Mage::getModel('neklo_asf/rates')->getCollection()
            ->addFieldToFilter('country', $cart['country'])
            ->addFieldToFilter('shipping_type', $shipType);
        return $collection;
    }

    public function calcMaxRate($sortMethods, $cart)
    {
        foreach ($sortMethods as $methodName => $rates) {
            if ($cart['SSP'] == Neklo_Asf_Model_Source_Specificprice::EXCLUDE) {
                $cart['shipping_type'][$methodName]['qty'] -=
                    $cart['shipping_type'][$methodName]['spec_qty'];
            }
            $maxSum = 0;
            foreach ($rates as $rate) {
                $rate['sum'] = $rate['per_order'] + ($rate['per_qty']
                        * $cart['shipping_type'][$methodName]['qty'])
                    + ($cart['price'] * ($rate['per_price'] / 100))
                    + ($cart['weight']
                        * $rate['per_weight']);
                if ($rate['sum'] >= $maxSum) {
                    $methodRates[$methodName] = $rate;
                    $maxSum = $rate['sum'];
                }
            }
        }
        return $methodRates;
    }
}