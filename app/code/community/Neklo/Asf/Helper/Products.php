<?php

class Neklo_Asf_Helper_Products extends Mage_Core_Helper_Abstract
{
    public function getInfoProduct($request)
    {
        $items = $request->getAllItems();
        foreach ($items as $item) {
            $productIds[] = $item->getProductId();
        }
        $productsCollection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addFieldToFilter('entity_id', array('in' => $productIds));
        $products = $productsCollection->getItems();
        foreach ($items as $item => $value) {
            if ($products[$value->getProductId()]->getTypeId()
                == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE
                || $products[$value->getProductId()]->getTypeId()
                == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE
            ) {
                $children = $value->getChildren();
                foreach ($children as $child => $info) {
                    $add = $info->getProduct()->getData()
                        + $products[$info->getProductId()]->getData();
                    $children[$child]->getProduct()->setData($add);
                    $items[$item]->setChildrenProduct($children);
                }

            }
            if ($value->getParentItemId() == null) {
                $add = $items[$item]->getProduct()->getData()
                    + $products[$value->getProductId()]->getData();
                $items[$item]->getProduct()->setData($add);
            }
        }
        $request->setAllItems($items);
        return $request;
    }

    public function getShippingType($request, $cart)
    {
        $items = $request->getAllItems();
        foreach ($items as $item) {
            $price = $item->getRowTotal();
            $weight = $item->getRowWeight();
            $qty = $item->getQty();
            $type = $item->getProduct()->getShippingType();
            if ($item->getProductType()
                == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE
                || $item->getProductType()
                == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE
            ) {
                $children = $item->getChildren();
                foreach ($children as $child) {
                    $childType = $child->getProduct()->getShippingType();
                    if ($childType === null) {
                        $cart['shipping_type'][0]['qty'] += $child->getQty();
                    } elseif ($childType !== null) {
                        $cart['shipping_type'][$childType]['qty']
                            += $child->getQty();
                        $cart['shipping_type'][$childType]['price']
                            += $child->getRowTotal();
                        $cart['shipping_type'][$childType]['weight']
                            += $child->getRowWeight();
                    } else {
                        $cart['shipping_type'][$childType]['qty']
                            += $child->getQty();
                    }
                }
                if ($cart['shipping_type'][0]['qty'] != null
                    && $cart['shipping_type'][0]['price'] == null
                ) {
                    $cart['shipping_type'][0]['price'] += $price;
                    $cart['shipping_type'][0]['weight'] += $weight;
                }
                continue;
            }
            if ($item->getParentItemId() == null) {
                if ($type === null) {
                    $cart['shipping_type'][0]['qty'] += $qty;
                    $cart['shipping_type'][0]['price'] += $price;
                    $cart['shipping_type'][0]['weight'] += $weight;
                } else {
                    $cart['shipping_type'][$type]['qty'] += $qty;
                    $cart['shipping_type'][$type]['price'] += $price;
                    $cart['shipping_type'][$type]['weight'] += $weight;
                }
            }
        }

        return $cart;
    }

    public function getVolume($request, $cart)
    {
        $dimensional = $cart['volume'];
        foreach ($request->getAllItems() as $item) {
            $type = $item->getProduct()->getShippingType();
            if ($type === null) {
                $type = 0;
            }
            $length = $item->getProduct()->getData($cart['length']);
            $width = $item->getProduct()->getData($cart['width']);
            $height = $item->getProduct()->getData($cart['height']);
            if ($item->getIsVirtual() == 1) {
                $cart['virtual_qty'] += $item->getQty();
            }
            if ($item->getProductType()
                == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE
                || $item->getProductType()
                == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE
            ) {
                $cart['shipping_type'][$type]['volume']
                    += ($length * $width * $height / $dimensional);
                $children = $item->getChildren();
                foreach ($children as $child) {
                    $cart['shipping_type'][$type]['volume']
                        += ($child->getProduct()->getData($cart['length'])
                        * $child->getProduct()->getData($cart['width'])
                        * $child->getProduct()->getData($cart['height'])
                        / $dimensional);
                }
                continue;
            }
            if ($item->getParentItemId() == null) {
                $cart['shipping_type'][$type]['volume']
                    += ($length * $width * $height / $dimensional);
            }
        }
        return $cart;
    }

    public function getSpecificPrice($request, $cart)
    {
        foreach ($request->getAllItems() as $item) {
            $ssp = $item->getProduct()->getSpecificShippingPrice();
            $type = $item->getProduct()->getShippingType();
            $qty = $item->getQty();
            if ($type === null) {
                $type = 0;
            }
            if ($item->getProductType()
                == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE
                || $item->getProductType()
                == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE
            ) {
                if ($ssp != null) {
                    $cart['shipping_type'][$type]['spec_qty'] += $qty;
                    $cart['shipping_type'][$type]['spec_sum'] += $qty * $ssp;
                }
                $children = $item->getChildren();
                foreach ($children as $child) {
                    if ($child->getProduct()->getSpecificShippingPrice()
                        != null
                    ) {
                        $cart['shipping_type'][$type]['spec_sum']
                            += $child->getQty()
                            * $child->getProduct()->getSpecificShippingPrice();
                    } else {
                        $cart['shipping_type'][$type]['spec_sum']
                            += $child->getQty() * $ssp;
                    }
                }
                continue;
            }
            if ($item->getParentItemId() == null && $ssp != null) {
                $cart['shipping_type'][$type]['spec_qty'] += $qty;
                $cart['shipping_type'][$type]['spec_sum'] += $qty * $ssp;
            }
        }
        return $cart;
    }
}