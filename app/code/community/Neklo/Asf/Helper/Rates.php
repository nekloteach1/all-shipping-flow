<?php

class Neklo_Asf_Helper_Rates extends Mage_Core_Helper_Abstract
{
    public function rates($methods, $cart)
    {
        $fields = array('subtotal_from', 'subtotal_to', 'qty_from', 
            'qty_to', 'weight_from', 'weight_to', 'volume_from', 'volume_to');
        foreach ($methods as $method => $rateId) {
            for ($i = 1; $i < 5; $i++) {
                $unSortMethods[$method][$i] = array();
            }
            $info = $cart['shipping_type'][$method];
            foreach ($rateId as $rate) {
                $check = $this->checkRate($fields, $rate, $info);
                if ($rate['state'] == $cart['state']
                    && $rate['city'] == $cart['city']
                    && $rate['zip_from'] <= $cart['zip']
                    && $rate['zip_to'] >= $cart['zip']
                ) {
                    if ($check == true) {
                        $unSortMethods[$method][1][$rate['id']] = $rate;
                    } else {
                        $unSortMethods[$method][2][$rate['id']] = $rate;
                    }
                    continue;
                } elseif ($check == true) {
                    $unSortMethods[$method][3][$rate['id']] = $rate;
                } else {
                    $unSortMethods[$method][4][$rate['id']] = $rate;
                }
            }
        }
        foreach ($unSortMethods as $method => $allPriority) {
            foreach ($allPriority as $rates) {
                if (!empty($rates)) {
                    $priority[$method] = $rates;
                    break;
                }
            }
        }
        foreach ($priority as $method => $rates) {
            $minNull = 8;
            $i = 0;
            foreach ($rates as $rate) {
                $rateNull = 0;
                foreach ($fields as $field) {
                    if($rate[$field] == 0)
                        $rateNull++;
                }
                if ($rateNull <= $minNull) {
                    if (!empty($sortMethods) && $rateNull < $minNull) {
                        unset($sortMethods[$method][$i - 1]);
                    }
                    $minNull = $rateNull;
                    $sortMethods[$method][$i] = $rate;
                    $i++;
                    unset($priority[$method][$rate['id']]);
                } else {
                    unset($priority[$method][$rate['id']]);
                }
            }
        }
        return $sortMethods;
    }

    public function checkRate($fields, $rate, $info)
    {
        $test = 0;
        for ($i = 0; $i<count($fields); $i+=2) {
            if($rate[$fields[$i]] <= $info[$fields[$i]]
            && $rate[$fields[$i+1]] >= $info[$fields[$i+1]])
                $test += 2;
        }
        if ($test == 8) {
            return true;
        } else {
            return false;
        }
    }

}