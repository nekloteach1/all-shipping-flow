<?php

class Neklo_Asf_Model_Carrier
    extends Mage_Shipping_Model_Carrier_Abstract
{
    protected $_code = 'nekloasf';

    public function collectRates(Mage_Shipping_Model_Rate_Request $request)
    {
        if (!$this->getConfigFlag('active')) {
            return false;
        }
        $result = Mage::getModel('shipping/rate_result');
        $cart = array('country'          => $request->getDestCountryId(),
                      'state'            => $request->getDestRegionCode(),
                      'city'             => $request->getDestCity(),
                      'zip'              => $request->getDestPostcode(),
                      'specific_country' => $this->getConfigData(
                          'specificcountry'
                      ),
                      'SSP'              => $this->getConfigData(
                          'specificshippingprice'
                      ),
                      'volume'           => $this->getConfigData(
                          'dimensionalfactor'
                      ),
                      'length'           => $this->getConfigData('length'),
                      'width'            => $this->getConfigData('width'),
                      'height'           => $this->getConfigData('height'),
                      'shipping_type'    => array(),
        );
        $methodCollection = Mage::helper('neklo_asf/carrier')->getMethods();
        if ($cart['specific_country'] != null) {
            $allowCountries = explode(
                ',', $cart['specific_country']
            );
            if (array_search($cart['country'], $allowCountries) === false) {
                return false;
            }
        }
        if ($cart['city'] == null) {
            $cart['city'] = '*';
        }
        if ($cart['state'] == null) {
            $cart['state'] = '*';
        }
        $request = Mage::helper('neklo_asf/products')
            ->getInfoProduct($request);
        $cart = Mage::helper('neklo_asf/products')
            ->getShippingType($request, $cart);
        $cart = Mage::helper('neklo_asf/products')
            ->getVolume($request, $cart);
        $cart = Mage::helper('neklo_asf/products')
            ->getSpecificPrice($request, $cart);
        foreach ($cart['shipping_type'] as $type => $value) {
            $rateCollection[$type] = Mage::helper('neklo_asf/carrier')
                ->getRates($cart, $type);
            if ($rateCollection[$type]->getItems() == null
            && empty($cart['shipping_type'][0])) {
                $rateCollection[0] = Mage::helper('neklo_asf/carrier')
                    ->getRates($cart, 0);
            }
        }
        foreach ($methodCollection as $method) {
            if ($method->getVirtualProduct() == 1) {
                $includeVirtual[$method['name']] = $cart['Virtual_Qty'];
            }
            foreach ($request->getAllItems() as $item) {
                if ($method->validate($item)) {
                    foreach ($rateCollection as $ship => $rates) {
                        foreach ($rates as $rateNum => $rate) {
                            if ($method['id'] == $rate['method_id']) {
                                $methods[$method['name']][$ship][$rate['id']]
                                    = $rate->getData();
                            }
                        }
                    }
                }
            }
        }
        foreach ($methods as $method => $shipTypes) {
            foreach ($cart['shipping_type'] as $ship => $value) {
                if (!isset($methods[$method][$ship])) {
                    $cart['shipping_type'][0]['qty'] +=
                        $cart['shipping_type'][$ship]['qty'];
                    $cart['shipping_type'][0]['price'] +=
                        $cart['shipping_type'][$ship]['price'];
                    $cart['shipping_type'][0]['weight'] +=
                        $cart['shipping_type'][$ship]['weight'];
                    $cart['shipping_type'][0]['volume'] +=
                        $cart['shipping_type'][$ship]['volume'];
                    $cart['shipping_type'][0]['spec_qty'] +=
                        $cart['shipping_type'][$ship]['spec_qty'];
                    $cart['shipping_type'][0]['spec_sum'] +=
                        $cart['shipping_type'][$ship]['spec_sum'];
                }
            }
        }
        foreach ($methods as $method => $shipTypes) {
            $sortMethods[$method] = Mage::helper('neklo_asf/rates')
                ->rates($methods[$method], $cart);
        }
        foreach ($sortMethods as $method => $shipTypes) {
            $methodRates[$method] = Mage::helper('neklo_asf/carrier')
                ->calcMaxRate($sortMethods[$method], $cart);
        }
        if ($methodRates != null) {
            $rates = $methodRates;
            $this->getMethods($rates, $result, $cart, $includeVirtual);
        } else {
            $error = Mage::getModel('shipping/rate_result_error');
            $error->setCarrier($this->_code);
            $error->setErrorMessage($this->getConfigData('specificerrmsg'));
            $result->append($error);
        }
        return $result;
    }


    protected function getMethods($rates, $result, $cart, $includeVirtual)
    {
        foreach ($rates as $nameMethod => $shipType) {
            if ($shipType != null) {
                $sum = 0;
                foreach ($shipType as $rate) {
                    $sum += $rate['sum'];
                    $sum += $cart['shipping_type']
                            [$rate['shipping_type']]['spec_sum'];
                }
                $includeVirtual[$nameMethod] *= $rate['per_qty'];
                $method = Mage::getModel('shipping/rate_result_method');
                $method->setCarrier($this->_code);
                $method->setCarrierTitle($this->getConfigData('title'));
                $method->setMethod($nameMethod);
                $method->setMethodTitle($nameMethod);
                $method->setPrice(
                    $sum + $includeVirtual[$nameMethod]
                );
                $result->append($method);
            }
        }
        return $result;
    }
}