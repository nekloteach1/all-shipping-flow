<?php

class Neklo_Asf_Model_Method extends Mage_Rule_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('neklo_asf/method');
    }

    public function getConditionsInstance()
    {
        return Mage::getModel('salesrule/rule_condition_combine');
    }

    public function getActionsInstance()
    {
        return Mage::getModel('salesrule/rule_condition_product_combine');
    }


}