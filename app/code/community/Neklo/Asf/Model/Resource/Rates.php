<?php

class Neklo_Asf_Model_Resource_Rates extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('neklo_asf/rates', 'id');
    }

    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);

        $select->joinLeft(
            array('methods' => 'neklo_asf_method'),
            $this->getMainTable() . '.method_id = methods.id',
            array('methods.name'));
        return $select;
    }
}