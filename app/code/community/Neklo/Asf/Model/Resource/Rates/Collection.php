<?php
class Neklo_Asf_Model_Resource_Rates_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('neklo_asf/rates');
    }
}