<?php

class Neklo_Asf_Model_Source_Country
{
    protected $_options;

    public function toOptionArray($allowCountry)
    {
        $options[] = array('label' => Mage::helper('neklo_asf')->__(
            'Please select a country'
        ));
        if ($allowCountry != null) {
            $country = explode(',', $allowCountry);
            $regions = Mage::getResourceModel('directory/country_collection')
                ->addFieldToFilter('country_id', array('in' => array($country)))
                ->load()
                ->toOptionArray(false);
            foreach ($regions as $region) {
                $options[] = $region;
            }

        }
        if ($allowCountry == null) {
            $regions = Mage::getResourceModel('directory/country_collection')
                ->load()
                ->toOptionArray(false);
            foreach ($regions as $region) {
                $options[] = $region;
            }
        }


        return $options;
    }
}
