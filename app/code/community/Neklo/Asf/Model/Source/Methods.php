<?php

class Neklo_Asf_Model_Source_Methods
{
    protected $_options;

    public function toOptionArray()
    {
        $this->_options[] = array('label' => 'Please select a method');
        $methods = Mage::getModel('neklo_asf/method')->getCollection();
        foreach ($methods->getData() as $method) {
            $this->_options[] = array('value' => $method['id'],
                                      'label' => $method['name']);
        }
        $options = $this->_options;

        return $options;
    }

    public function sortRegionCountries($a, $b)
    {
        return strcmp($this->_countries[$a], $this->_countries[$b]);
    }
}
