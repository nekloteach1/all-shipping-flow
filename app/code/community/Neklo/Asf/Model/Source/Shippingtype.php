<?php

class Neklo_Asf_Model_Source_Shippingtype extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    const NONE = '0';
    const FRAGILE = '1';
    const HEAVY = '2';
    const TOXIC = '3';

    public function getAllOptions()
    {
        return array(
            array('value' => self::NONE,
                  'label' => '*'),
            array('value' => self::FRAGILE,
                  'label' => Mage::helper('neklo_asf')->__('Fragile')),
            array('value' => self::HEAVY,
                  'label' => Mage::helper('neklo_asf')->__('Heavy')),
            array('value' => self::TOXIC,
                  'label' => Mage::helper('neklo_asf')->__('Toxic')),
        );
    }
    public function toOptionArray()
    {
        return array(
            array('value' => self::NONE,
                  'label' => '*'),
            array('value' => self::FRAGILE,
                  'label' => Mage::helper('neklo_asf')->__('Fragile')),
            array('value' => self::HEAVY,
                  'label' => Mage::helper('neklo_asf')->__('Heavy')),
            array('value' => self::TOXIC,
                  'label' => Mage::helper('neklo_asf')->__('Toxic')),
        );
    }
    public function toArray()
    {
        return array(
            self::NONE => Mage::helper('neklo_asf')->__('*'),
            self::FRAGILE  => Mage::helper('neklo_asf')->__('Fragile'),
            self::HEAVY  => Mage::helper('neklo_asf')->__('Heavy'),
            self::TOXIC  => Mage::helper('neklo_asf')->__('Toxic'),
        );
    }
}