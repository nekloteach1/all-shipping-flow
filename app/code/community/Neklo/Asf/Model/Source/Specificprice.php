<?php

class Neklo_Asf_Model_Source_Specificprice
{
    const EXCLUDE = '0';
    const EXTRA = '1';

    public function toOptionArray()
    {
        return array(
            array('value' => self::EXCLUDE,
                  'label' => Mage::helper('neklo_asf')->__('Exclude product with specific shipping price from calculation')),
            array('value' => self::EXTRA,
                  'label' => Mage::helper('neklo_asf')->__('Extra charge for product with specific shipping price')),
        );
    }
}