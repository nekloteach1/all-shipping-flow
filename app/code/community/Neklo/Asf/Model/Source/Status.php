<?php

class Neklo_Asf_Model_Source_Status
{
    const ENABLED = '1';
    const DISABLED = '0';

    public function toOptionArray()
    {
        return array(
            array('value' => self::ENABLED,
                  'label' => Mage::helper('neklo_asf')->__('Enabled')),
            array('value' => self::DISABLED,
                  'label' => Mage::helper('neklo_asf')->__('Disabled')),
        );
    }

    public function toArray()
    {
        return array(
            self::DISABLED => Mage::helper('neklo_asf')->__('Disabled'),
            self::ENABLED  => Mage::helper('neklo_asf')->__('Enabled'),
        );
    }
}