<?php

class Neklo_Asf_Model_Source_Virtual
{
    const ENABLED = '1';
    const DISABLED = '0';

    public function toOptionArray()
    {
        return array(
            array('value' => self::ENABLED,
                  'label' => Mage::helper('neklo_asf')->__('Yes')),
            array('value' => self::DISABLED,
                  'label' => Mage::helper('neklo_asf')->__('No')),
        );
    }

    public function toArray()
    {
        return array(
            self::ENABLED  => Mage::helper('neklo_asf')->__('Yes'),
            self::DISABLED => Mage::helper('neklo_asf')->__('No'),
        );
    }
}