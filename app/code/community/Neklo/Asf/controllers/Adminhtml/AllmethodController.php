<?php

class Neklo_Asf_Adminhtml_AllmethodController
    extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed(
            'sales/neklo_asf/methods'
        );
    }

    public function indexAction()
    {
        $this->loadLayout()->_title($this->__('Shipping Methods Management'));
        $this->_addContent(
            $this->getLayout()->createBlock('neklo_asf/adminhtml_allmethod')
        );
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        Mage::register(
            'nekloasf_method', Mage::getModel('neklo_asf/method')->load($id)
        );
        $methodObject = (array)Mage::getSingleton('adminhtml/session')
            ->getFormData();
        if (count($methodObject)) {
            Mage::registry('nekloasf_method')->setData($methodObject);
        }
        $this->loadLayout()->_title($this->__('Shipping Method Manage'));
        $this->getLayout()->getBlock('head')
            ->setCanLoadExtJs(true)
            ->setCanLoadRulesJs(true);

        $this->_addLeft(
            $this->getLayout()->createBlock(
                'neklo_asf/adminhtml_allmethod_edit_tabs'
            )
        );
        $this->_addContent(
            $this->getLayout()->createBlock(
                'neklo_asf/adminhtml_allmethod_edit'
            )
        );
        $this->renderLayout();
    }

    public function saveAction()
    {
        try {
            $id = $this->getRequest()->getParam('id');
            $method = Mage::getModel('neklo_asf/method')->load($id);
            $data = $this->getRequest()->getParams();
            if (isset($data['rule']['conditions'])) {
                $data['conditions'] = $data['rule']['conditions'];
                unset($data['rule']);
            }
            $method->loadPost($data)
                ->save();
            if (!$method->getId()) {
                Mage::getSingleton('adminhtml/session')
                    ->addError('Cannot save the method');
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setBlockObject(
                $method->getData()
            );
        }
        Mage::getSingleton('adminhtml/session')
            ->addSuccess('Method was saved successfully!');
        $this->_redirect(
            '*/*/' . $this->getRequest()
                ->getParam('back', 'index'), array('id' => $method->getId())
        );
    }

    public function deleteAction()
    {
        $method = Mage::getModel('neklo_asf/method')
            ->setId($this->getRequest()->getParam('id'))
            ->delete();
        if ($method->getId()) {
            Mage::getSingleton('adminhtml/session')->addSuccess(
                'Method was deleted successfully!'
            );
        }
        $this->_redirect('*/*/');
    }

    public function addrateAction()
    {
        $this->_redirect(
            '*/allrates/new/method/' . $this->getRequest()->getParam('id')
        );
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock(
                'neklo_asf/adminhtml_allmethod_edit_tab_rates'
            )->toHtml()
        );
    }
}