<?php

class Neklo_Asf_Adminhtml_AllratesController
    extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed(
            'sales/neklo_asf/rates'
        );
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('id');
        Mage::register(
            'rates', Mage::getModel('neklo_asf/rates')->load($id)
        );
        $this->loadLayout()->_title(
            $this->__('%s Rate Manage', Mage::registry('rates')->getName())
        );
        $this->_addContent(
            $this->getLayout()->createBlock('neklo_asf/adminhtml_allrates_edit')
        );
        $this->renderLayout();

    }

    public function saveAction()
    {
        try {
            $id = $this->getRequest()->getParam('id');
            $rate = Mage::getModel('neklo_asf/rates')->load($id);
            $rate->setCountry($this->getRequest()->getParam('country'))
                ->setCity($this->getRequest()->getParam('city'))
                ->setState($this->getRequest()->getParam('state'))
                ->setZipFrom($this->getRequest()->getParam('zip_from'))
                ->setZipTo($this->getRequest()->getParam('zip_to'))
                ->setSubtotalFrom(
                    $this->getRequest()->getParam('subtotal_from')
                )
                ->setSubtotalTo(
                    $this->getRequest()->getParam('subtotal_to')
                )
                ->setQtyFrom($this->getRequest()->getParam('qty_from'))
                ->setQtyTo($this->getRequest()->getParam('qty_to'))
                ->setWeightFrom(
                    $this->getRequest()->getParam('weight_from')
                )
                ->setWeightTo($this->getRequest()->getParam('weight_to'))
                ->setPerOrder($this->getRequest()->getParam('per_order'))
                ->setPerQty($this->getRequest()->getParam('per_qty'))
                ->setPerPrice($this->getRequest()->getParam('per_price'))
                ->setPerWeight($this->getRequest()->getParam('per_weight'))
                ->setMethodId($this->getRequest()->getParam('method_id'))
                ->setShippingType($this->getRequest()->getParam('shipping_type'))
                ->setVolumeFrom($this->getRequest()->getParam('volume_from'))
                ->setVolumeTo($this->getRequest()->getParam('volume_to'));
            $rate->save();
            if (!$rate->getId()) {
                Mage::getSingleton('adminhtml/session')->addError(
                    'Cannot save the rate'
                );
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setBlockObject(
                $rate->getData()
            );
        }
        Mage::getSingleton('adminhtml/session')->addSuccess(
            'Rate was saved successfully!'
        );
        $this->_redirect(
            '*/allmethod/' . $this->getRequest()->getParam('back', 'edit'),
            array('id' => $rate->getMethodId())
        );

    }

    public function deleteAction()
    {
        $method = Mage::getModel('neklo_asf/rates')
            ->setId($this->getRequest()->getParam('id'))
            ->delete();
        if ($method->getId()) {
            Mage::getSingleton('adminhtml/session')->addSuccess(
                'Rate was deleted successfully!'
            );
        }
        $this->_redirect(
            '*/allmethod/' . $this->getRequest()->getParam('back', 'index')
        );
    }

}