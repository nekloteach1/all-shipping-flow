<?php
/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();
$installer->run("
CREATE TABLE IF NOT EXISTS `{$this->getTable('neklo_asf/method')}` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `name` varchar(255) NOT NULL,
  `active` char(1) NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `{$this->getTable('neklo_asf/rates')}` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `method_id` int(11) NOT NULL,
  `country` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip_from` varchar(255) NOT NULL,
  `zip_to` varchar(255) NOT NULL,
  `subtotal_from` decimal(12,4) NOT NULL,
  `subtotal_to` decimal(12,4) NOT NULL,
  `qty_from` decimal(12,4) NOT NULL, 
  `qty_to` decimal(12,4) NOT NULL, 
  `weight_from` decimal(12,4) NOT NULL,
  `weight_to` decimal(12,4) NOT NULL,  
  `per_order` decimal(12,4) NOT NULL,
  `per_qty` decimal(12,4) NOT NULL,
  `per_price` decimal(12,4)  NOT NULL,
  `per_weight` decimal(12,4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
SET FOREIGN_KEY_CHECKS = 0;
ALTER TABLE `{$this->getTable('neklo_asf/rates')}` 
ADD  FOREIGN KEY (`method_id`) REFERENCES {$this->getTable('neklo_asf/method')}(`id`) 
ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS = 1;

");
$installer->endSetup();