<?php
$installer = $this;
$installer->startSetup();
$installer->run(
    "ALTER TABLE `{$this->getTable('neklo_asf/method')}` 
ADD `conditions_serialized` TEXT NOT NULL AFTER `active`; 
"
);
$installer->endSetup();