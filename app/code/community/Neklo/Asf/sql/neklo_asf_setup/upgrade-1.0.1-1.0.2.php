<?php
$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();
$installer->addAttribute('catalog_product', 'specific_shipping_price', array(
    'group'             => 'Prices',
    'type'              => 'decimal',
    'backend'           => '',
    'frontend'          => '',
    'label'             => 'Specific Shipping Price',
    'input'             => 'price',
    'class'             => '',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'           => false,
    'required'          => false,
    'user_defined'      => false,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,bundle,grouped',
    'is_configurable'   => false,
));
$installer->endSetup();
