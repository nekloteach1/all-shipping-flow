<?php
$installer = $this;
$installer->startSetup();
$installer->run(
    "ALTER TABLE `{$this->getTable('neklo_asf/method')}` 
ADD `virtual_product` char(1) NOT NULL AFTER `conditions_serialized`; 
"
);
$installer->endSetup();