<?php
$installer = $this;
$installer->startSetup();
$installer->run(
    "ALTER TABLE `{$this->getTable('neklo_asf/rates')}` 
ADD `shipping_type` int(1) NOT NULL AFTER `per_weight`; 
");
$installer->endSetup();

$installer = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();
$installer->addAttribute('catalog_product', 'shipping_type', array(
    'group'             => 'General',
    'type'              => 'int',
    'backend'           => '',
    'frontend'          => '',
    'label'             => 'Shipping Type',
    'input'             => 'select',
    'class'             => '',
    'source'			=> 'neklo_asf/source_shippingtype',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'           => false,
    'required'          => false,
    'user_defined'      => false,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'  => false,
    'unique'            => false,
    'apply_to'          => 'simple,configurable,bundle,grouped',
    'is_configurable'   => false,
    'default'           => 0,
));
$installer->endSetup();
