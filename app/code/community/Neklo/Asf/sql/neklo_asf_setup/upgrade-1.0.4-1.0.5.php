<?php
$installer = $this;
$installer->startSetup();
$installer->run(
    "ALTER TABLE `{$this->getTable('neklo_asf/rates')}` 
ADD `volume_from` decimal(12,4) NOT NULL AFTER `shipping_type`, 
ADD `volume_to` decimal(12,4) NOT NULL AFTER `volume_from`;
"
);
$installer->endSetup();